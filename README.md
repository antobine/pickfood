<p align="center">
<img src="https://prismaestudio.com.ar/storage/pickfood_tn/logo-full.svg" width="200px">
</p>


## About the project

Pick Food is a food delivery company. The website offers promotions and the menu for clients.

## Built with

- Laravel
- Livewire
- Tailwind CSS
