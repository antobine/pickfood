<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    protected $mName = 'categorias';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.'.$this->mName.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateFields();
        
        $new = new Categoria;

        $new->nombre  = $request->nombre;
        $new->color   = $request->color;
        
        $new->save();

        return redirect()->route('admin.'.$this->mName);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
        return view('admin.'.$this->mName.'.edit', compact(['categoria']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        $this->validateFields();

        $categoria->nombre   = $request->nombre;
        $categoria->color    = $request->color;
        
        $categoria->save();

        return redirect()->route('admin.'.$this->mName);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {
        $categoria->delete();
        return redirect()->route('admin.'.$this->mName)->with('mensaje', 'La categoria fue eliminado.');
    }


    protected function validateFields()
    {
       return request()->validate([
            'nombre'     => 'required|string|max:190',
            'color'      => 'nullable|string'
        ]);
    }

}
