<?php

namespace App\Http\Controllers;

use App\Models\Plato;
use App\Models\Categoria;
use Illuminate\Http\Request;

use App\Models\Upload;

class PlatoController extends Controller
{
    protected $mName = 'platos';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();
        return view('admin.'.$this->mName.'.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateFields();
        $this->validate(request(), [
            'photo' => 'nullable|image|max:5024',
        ]);
        
        if( $request->photo ){
            $path = Upload::uploadPhoto( $request->photo, 'storage/'.$this->mName.'/', 600, 600 );
        }
        
        $new = new Plato;

        $new->photo_path = $path;

        $new->nombre       = $request->nombre;
        $new->detalle      = $request->detalle;
        $new->calorias     = $request->calorias;
        $new->peso         = $request->peso;
        $new->alt          = $request->alt;
        $new->precio       = $request->precio;
        $new->is_visible   = $request->is_visible;
        $new->is_destacado = $request->is_destacado;
        $new->categoria_id = $request->categoria_id;
        
        $new->save();

        return redirect()->route('admin.'.$this->mName);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plato  $plato
     * @return \Illuminate\Http\Response
     */
    public function show(Plato $plato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plato  $plato
     * @return \Illuminate\Http\Response
     */
    public function edit(Plato $plato)
    {
        $categorias = Categoria::all();
        return view('admin.'.$this->mName.'.edit', compact(['plato', 'categorias']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plato  $plato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plato $plato)
    {
        $this->validateFields();
        $this->validate(request(), [
            'photo' => 'nullable|image|max:5024',
        ]);
        
        if( $request->photo ){
            $path = Upload::uploadPhoto( $request->photo, 'storage/'.$this->mName.'/', 600, 600 );
            $plato->photo_path = $path;
        }

        $plato->nombre     = $request->nombre;
        $plato->detalle    = $request->detalle;
        $plato->calorias     = $request->calorias;
        $plato->peso         = $request->peso;
        $plato->alt          = $request->alt;
        $plato->precio       = $request->precio;
        $plato->is_visible   = $request->is_visible;
        $plato->is_destacado = $request->is_destacado;
        $plato->categoria_id = $request->categoria_id;
        
        $plato->save();

        return redirect()->route('admin.'.$this->mName);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plato  $plato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plato $plato)
    {
        $plato->delete();
        return redirect()->route('admin.'.$this->mName)->with('mensaje', 'El plato fue eliminado.');
    }

    
    protected function validateFields()
    {
       return request()->validate([
            'nombre'       => 'required|string|max:190',
            'detalle'      => 'nullable|string',
            'calorias'     => 'nullable|string',
            'peso'         => 'nullable|string',
            'precio'       => 'integer|required',
            'alt'          => 'string|nullable',
            'categoria_id' => 'exists:categorias,id',
            'is_visible'   => 'required|boolean',
            'is_destacado' => 'nullable|boolean'
        ]);
    }

}
