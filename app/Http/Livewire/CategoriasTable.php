<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Categoria;

class CategoriasTable extends Component
{
    public function render()
    {
        $categorias = Categoria::orderBy('created_at', 'ASC')
                               ->get();


        return view('livewire.categorias-table', [
            'categorias' => $categorias
        ]);
    }
}
