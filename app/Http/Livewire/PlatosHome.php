<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Plato;

class PlatosHome extends Component
{
    public $limit = true;

    public $platos;
    public $original;
    private $resto;

    public function more()
    {
       $this->limit = false;
    }

    public function appendItems()
    {
        $this->original = $this->platos;
        $this->resto = Plato::where('is_destacado', null)->get();
        $this->platos = $this->original->merge($this->resto); 
    }

    public function render()
    {
        if( $this->limit ){
            $this->platos = Plato::where('is_destacado', 1)
                            ->take(8)
                            ->inRandomOrder()
                            ->get();
        }else{
            $this->appendItems();
        }
        return view('livewire.platos-home', [
            'platos' => $this->platos
        ]);
    }
}
