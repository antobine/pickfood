<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Plato;

class PlatosTable extends Component
{

    public function render()
    {
        $platos = Plato::withoutGlobalScope('visible')
                                ->orderBy('created_at', 'ASC')->get();

        return view('livewire.platos-table', [
            'platos' => $platos
        ]);
    }
}
