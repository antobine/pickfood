<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;

class UploadPhoto extends Component
{
    use WithFileUploads;

    public $photo;
    public $text;

    public function render()
    {
        return view('livewire.upload-photo');
    }

    public function updatedPhoto()
    {
        $this->validate([
            'photo' => 'image|max:4024'
        ]);

    }

}
