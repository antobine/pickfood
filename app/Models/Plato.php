<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plato extends Model
{
    use HasFactory;

    protected static function booted()
    {
        static::addGlobalScope('visible', function ($builder) {
            $builder->where('is_visible', 1);
        });
    }

    public function categoria()
    {
       return $this->belongsTo( Categoria::class );
    }
}
