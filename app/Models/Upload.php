<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Image;

class Upload extends Model
{
    public static function uploadPhoto( $image, $route, $width, $height ){

        $filename  = time() . '.' . $image->getClientOriginalExtension();

       // DEV
       $path = public_path($route . $filename);
       // PRODUCTION  $path = $route . $filename;

        Image::make($image->getRealPath())->fit($width, $height)->save($path);

        return $filename;

    }
    
    public static function uploadPdf( $file, $route, $slug ){

        $filename = $slug . '-' . time() . '.' . $file->getClientOriginalExtension();

        Storage::putFileAs('public/'.$route, $file, $filename);

        return $filename;

    }
}
