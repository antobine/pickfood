/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : pickfood

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-01-27 12:19:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for platos
-- ----------------------------
DROP TABLE IF EXISTS `platos`;
CREATE TABLE `platos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detalle` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calorias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `peso` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` int(11) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoria_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `platos_categoria_id_foreign` (`categoria_id`),
  CONSTRAINT `platos_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of platos
-- ----------------------------
INSERT INTO `platos` VALUES ('2', 'Bondiola braseada con batatas al horno', 'Bondiola, batata, cebolla, puerro, zanahoria, apio, ajo, vino tinto, caldo, aceite, sal.', null, '450gr.', '1611756768.png', 'bondiola de cerdo con salsa y batatas', '490', '1', '2021-01-27 11:12:48', '2021-01-27 11:12:48', '1');
INSERT INTO `platos` VALUES ('3', 'Bondiola braseada con papas al horno', 'Bondiola, papa, cebolla, puerro, zanahoria, apio, ajo, vino tinto, caldo, aceite, sal.', null, '450gr.', '1611757138.png', 'bondiola de cerdo con salsa y papas', '490', '1', '2021-01-27 11:18:58', '2021-01-27 11:18:58', '1');
INSERT INTO `platos` VALUES ('4', 'Ribs de cerdo con batatas bravas', 'Ribs de cerdo, batata, zanahoria, cebolla, apio, vino blanco, caldo, barbacoa, aceite, pimentón, ají molido, comino, orégano, ajo, pimienta y sal.', null, '480gr.', '1611757314.png', 'ribs de cerdo con barbacoa y batatas bravas', '670', '1', '2021-01-27 11:21:54', '2021-01-27 11:21:54', '1');
INSERT INTO `platos` VALUES ('5', 'Ribs de cerdo con papas bravas', 'Ribs de cerdo, zanahoria, cebolla, papa, apio, vino blanco, caldo, barbacoa, aceite pimentón, ají molido, comino, orégano, ajo, pimienta y sal.', null, '480gr.', '1611757359.png', 'ribs de cerdo con barbacoa y papas bravas', '670', '1', '2021-01-27 11:22:40', '2021-01-27 11:22:40', '1');
INSERT INTO `platos` VALUES ('6', 'Salmón grillado con puré de papas e hinojos', 'Salmón, papa, hinojo, ajo, leche, aceite, sal.', null, '400gr.', '1611757512.png', 'salmon grillado en plancha con pure de papa e hinojos', '640', '1', '2021-01-27 11:25:12', '2021-01-27 11:25:12', '3');
INSERT INTO `platos` VALUES ('7', 'Fricasse de pollo con arroz yamaní', 'Pollo, arroz yamani, zanahoria, morrón, puerro, cebolla, champiñón, aceite, vino, cúrcuma, pimienta, sal, manteca, harina.', null, '430gr.', '1611757639.png', 'pollo salteado en salsa con arroz y especias', '390', '1', '2021-01-27 11:27:19', '2021-01-27 11:27:19', '2');
INSERT INTO `platos` VALUES ('8', 'Burger con papas bravas', 'Carne vacuna, papa, cebolla, ajo, perejil, aceite, orégano, pimentón dulce, ají molido, comino, pimienta y sal.', null, '480gr.', '1611757788.png', 'hamburguesas de carne con papas bravas', '470', '1', '2021-01-27 11:29:48', '2021-01-27 11:29:48', '1');
INSERT INTO `platos` VALUES ('9', 'Albóndigas a la pomarola con puré de papas', 'Carne de ternera, papa, zanahoria, cebolla, cebolla de verdeo, perejil, huevo, pan rallado, romero,  orégano, ajo, azúcar, aceite, salsa de tomate y sal.', null, '480gr.', '1611757948.png', 'albondigas con salsa pomarola y cremoso de papa', '380', '1', '2021-01-27 11:32:28', '2021-01-27 11:32:28', '1');
INSERT INTO `platos` VALUES ('10', 'Matambre a la pizza con papas al horno', 'Matambre vacuno, papa, queso, salsa de tomate, morrón rojo, cebolla, ajo, orégano, laurel, aceite, fécula de maíz, pimienta y sal.', null, '460gr.', '1611758088.png', 'matambre tiernizado con queso, salsa y papas', '545', '1', '2021-01-27 11:34:49', '2021-01-27 11:34:49', '1');
INSERT INTO `platos` VALUES ('11', 'Chorizo a la pomarola con papas al horno', 'Chorizo de cerdo, papa, cebolla, zanahoria, tomate, morrón rojo, morrón verde, cebolla de verdeo, laurel, aceite, vino blanco, extracto de tomate, sal.', null, '450gr.', '1611758201.png', 'Chorizo a la pomarola con cremoso de papas', '390', '1', '2021-01-27 11:36:42', '2021-01-27 11:36:42', '1');
INSERT INTO `platos` VALUES ('12', 'Pollo roti con calabazas al horno', 'Pollo, calabaza, aceite, orégano, pimienta y sal.', null, '450gr.', '1611758277.png', 'pollo roti con calabazas asadas al horno', '390', '1', '2021-01-27 11:37:57', '2021-01-27 11:37:57', '2');
INSERT INTO `platos` VALUES ('13', 'Pollo roti con papas al horno', 'Pollo, papa, aceite, orégano pimienta y sal.', null, '450gr.', '1611758323.png', 'pollo roti con papas asadas al horno', '390', '1', '2021-01-27 11:38:43', '2021-01-27 11:38:43', '2');
INSERT INTO `platos` VALUES ('14', 'Risotto al funghi', 'Arroz carnaroli, hongos de pino, champignon paris, zanahoria, cebolla, puerro, morrón, vino blanco, manteca, queso, aceite, pimienta, sal.', null, '450gr.', '1611758481.png', 'risotto con hongos', '480', '1', '2021-01-27 11:41:21', '2021-01-27 11:41:21', '4');
INSERT INTO `platos` VALUES ('15', 'Risotto de quinoa', 'Quinoa, morrón, cebolla, zanahoria, puerro, apio, aceite, pimienta y sal.', null, '400gr.', '1611758542.png', 'risoto de quinoa y verduras', '360', '1', '2021-01-27 11:42:22', '2021-01-27 11:42:22', '4');
INSERT INTO `platos` VALUES ('16', 'Goulash con Spaetzle', 'Roast beef, cebolla, caldo de verdura, salsa de tomate, vino blanco, harina, huevos, leche, pimienta y sal.', null, '480gr.', '1611758589.png', 'goulash con spaetzle', '395', '1', '2021-01-27 11:43:09', '2021-01-27 11:43:09', '1');
INSERT INTO `platos` VALUES ('17', 'Risotto di mare', 'Trigo pelado, vieiras, langostinos, zanahoria, morrón, puerro, apio, cebolla, caldo de verdura, queso, manteca, cáscara de naranja, aceite y sal.', null, '450gr.', '1611758699.png', 'risotto di mare con vieiras y langostinos', '520', '1', '2021-01-27 11:45:00', '2021-01-27 11:45:00', '3');
INSERT INTO `platos` VALUES ('18', 'Veggie burgers con batatas al horno', 'Lentejas, arroz yamaní, batata, zanahoria, perejil picado, ajo, romero picado, aceite de oliva, comino, lino, chía, pimienta y sal.', null, '430gr.', '1611758802.png', 'hamburguesas veganas de lentejas con batatas al horno', '380', '1', '2021-01-27 11:46:42', '2021-01-27 11:46:42', '4');
INSERT INTO `platos` VALUES ('19', 'Wok de soja texturizada', 'Soja texturizada, morrón rojo, zanahorias, cebolla, berenjenas, cebolla de verdeo, jengibre, ajo, salsa de soja, salsa teriyaki, aceite de maíz, aceite de sésamo, almidón de maíz, sal.', null, '390gr.', '1611758874.png', 'wok de soja texturizada verduras brotes de soja semillas y salsa', '370', '1', '2021-01-27 11:47:54', '2021-01-27 11:47:54', '4');
