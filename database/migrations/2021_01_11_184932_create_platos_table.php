<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('detalle')->nullable();
            $table->string('calorias')->nullable();
            $table->string('peso')->nullable();
            $table->string('photo_path');
            $table->string('alt')->nullable();
            $table->integer('precio');
            $table->boolean('is_destacado')->default(0);
            $table->boolean('is_visible')->default(1);
            $table->timestamps();

            $table->foreignId('categoria_id')->constrained('categorias')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platos');
    }
}
