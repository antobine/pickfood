<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'nombre'     => 'Carne',
            'color'    => 'violeta'
        ]);

        DB::table('categorias')->insert([
            'nombre'     => 'Pollo',
            'color'    => 'naranja'
        ]);
        
        DB::table('categorias')->insert([
            'nombre'     => 'Pescado',
            'color'    => 'naranja'
        ]);

        DB::table('categorias')->insert([
            'nombre'     => 'Veggie',
            'color'    => 'verde'
        ]);
    }
}
