<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Prisma Design',
            'email'    => 'info@prismadesign.com.ar',
            'password' => Hash::make('Cunx*5Ls-')
        ]);

        DB::table('users')->insert([
            'name'     => 'Silvio Mendoza',
            'email'    => 'micaela.karaman@gmail.com',
            'password' => Hash::make('Luma/024')
        ]);

    }
}
