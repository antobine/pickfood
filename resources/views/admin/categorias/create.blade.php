@php
	$routeName = 'categorias';
	$mName = 'categoria';
@endphp

<x-app-layout>

	<x-slot name="headtags">
		<title>Crear {{ $mName }} - Admin {{ config('app.name') }}</title>
	</x-slot>


	<div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">

		<x-slot name="header">
			<h2 class="text-sm text-gray-400 uppercase">{{ ucfirst($routeName) }}</h2>
			<h1 class="block text-2xl md:text-5xl leading-none mb-4">Nuevo {{ $mName }}</h1>
		</x-slot>

		<x-formmsg />
		{{-- @include('admin.partials.valid_msg') --}}

		
		<form action="/{{ $routeName }}" method="POST" enctype="multipart/form-data"
				class="w-full">

			{{ csrf_field() }}

			<div class="md:flex md:pt-16">

				<div class="md:w-2/3 md:pr-16">
		
					<div class="mb-2">
						<x-jet-label for="nombre" value="Nombre*" />
						<x-field id="nombre" class="block mt-1 w-full" type="text" name="nombre" :value="old('nombre')" autofocus required />
					</div>

					<div class="mb-2">
						<x-jet-label for="color" value="color*" />
						<x-field id="color" class="block mt-1 w-full" type="text" name="color" :value="old('color')" />
					</div>
                    

				</div>
				{{-- end col-first --}}


				<div class="md:w-1/3">


				</div>
				{{-- end col-4 --}}
			</div>


			<div class="md:flex md:justify-between py-16">

				<a href="{{ route('admin.'.$routeName) }}" class="font-bold">Cancelar</a>
						
				<x-submit type="submit"
						class="btn-shadow-fucsia">Crear {{$mName }}</x-submit>

			</div>


		</form>

	</div>


	<x-slot name="scripts">

	</x-slot>

</x-app-layout>