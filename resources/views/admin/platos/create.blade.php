@php
	$routeName = 'platos';
	$mName = 'plato';
@endphp

<x-app-layout>

	<x-slot name="headtags">
		<title>Crear {{ $mName }} - Admin {{ config('app.name') }}</title>
	</x-slot>


	<div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">

		<x-slot name="header">
			<h2 class="text-sm text-gray-400 uppercase">{{ ucfirst($routeName) }}</h2>
			<h1 class="block text-2xl md:text-5xl leading-none mb-4">Nuevo {{ $mName }}</h1>
		</x-slot>

		<x-formmsg />
		{{-- @include('admin.partials.valid_msg') --}}

		
		<form action="/{{ $routeName }}" method="POST" enctype="multipart/form-data"
				class="w-full">

			{{ csrf_field() }}

			<div class="md:flex md:pt-16">

				<div class="md:w-2/3 md:pr-16">
		
					<div class="mb-2">
						<x-jet-label for="nombre" value="Nombre*" />
						<x-field id="nombre" class="block mt-1 w-full" type="text" name="nombre" :value="old('nombre')" autofocus required />
					</div>

					<fieldset class="mb-2">
						<x-jet-label for="categoria_id" value="Categoría" class="mr-4" />
						<select name="categoria_id" id="categoria_id" class="w-full rounded border-gray-200">
						@foreach ($categorias as $cat)
	
							<option value="{{ $cat->id }}">{{ Str::ucfirst($cat->nombre) }}</option>
							
						@endforeach
						</select>
					</fieldset>

					<div class="mb-2">
						<x-jet-label for="precio" value="Precio*" />
						<x-field id="precio" class="block mt-1 w-full" type="text" name="precio" :value="old('precio')" />
					</div>

					<x-divisor />

					<div class="mb-2">
						<x-jet-label for="detalle" value="Detalle" />
						<x-textarea name="detalle" id="detalle" cols="10" rows="4"
								class="w-full short_desc-editor">{{ old('detalle') }}</x-textarea>
					</div>

					<fieldset class="mb-2 md:flex">
						<div class="md:w-1/2 md:mr-2">
							<x-jet-label for="calorias" value="calorias" />
							<x-field id="calorias" class="block mt-1 w-full" type="text" name="calorias" :value="old('calorias')" />
						</div>
						
						<div class="md:w-1/2 ">
							<x-jet-label for="peso" value="peso" />
							<x-field id="peso" class="block mt-1 w-full" type="text" name="peso" :value="old('peso')" />
						</div>
					</fieldset>
                    

				</div>
				{{-- end col-first --}}


				<div class="md:w-1/3">

					<x-jet-label for="photo_main" value="Imagen principal" />
                    @livewire('upload-photo', ['text' => 'Seleccionar imagen'])
                    

                    <div class="mb-2">
						<x-jet-label for="alt" value="Textp alt (metele keywords!)" />
						<x-field id="alt" class="block mt-1 w-full" type="text" name="alt" :value="old('alt')" />
					</div>


					<fieldset class="mb-2">
						<x-jet-label for="visible" value="Visible" />

						<label class="radio">
							<input type="radio" name="is_visible" value="1" checked>
							Si
						</label>
						<label class="radio">
							<input type="radio" name="is_visible" value="0">
							No
						</label>
					</fieldset>
					
					<fieldset class="mb-2">
						<x-jet-label for="destacado" value="destacado" />

						<label class="radio">
							<input type="radio" name="is_destacado" value="1">
							Si
						</label>
						<label class="radio">
							<input type="radio" name="is_destacado" value="0" checked>
							No
						</label>
					</fieldset>

				</div>
				{{-- end col-4 --}}
			</div>


			<div class="md:flex md:justify-between py-16">

				<a href="{{ route($routeName) }}" class="font-bold">Cancelar</a>
						
				<x-submit type="submit"
						class="btn-shadow-fucsia">Crear {{$mName }}</x-submit>

			</div>


		</form>

	</div>


	<x-slot name="scripts">

	</x-slot>

</x-app-layout>