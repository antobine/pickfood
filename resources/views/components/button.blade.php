<a {{ $attributes->merge(['href' => '', 'class' => 'btn btn-primary block']) }}
    ><span class="btn-text">{{ $slot }}</span></a>