<label {{ $attributes->merge(['class' => 'flex items-center mb-2']) }}>
    <input type="checkbox" class="form-checkbox" name="{{ $inputname }}" value="{{ $value }}" {{ $checked ? 'checked' : ''}}>
    <span class="ml-2">{{ $slot }}</span>
</label>