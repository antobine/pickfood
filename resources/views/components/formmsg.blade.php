@if( count($errors) )
	<div class="container mx-auto bg-primary rounded text-white font-bold p-6 my-4">

			<ul class="pl-6 list-disc">

				@foreach ($errors->all() as $error)

					<li>{{ $error }}</li>

				@endforeach

			</ul>

	</div>
@endif