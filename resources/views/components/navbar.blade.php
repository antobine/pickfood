<div x-data="{ isOpen: false, isFixed: false }"
    @scroll.window="isFixed = (window.pageYOffset > 200) ? true : false">
    
    <header class="w-full z-50 shadow transition-all" :class="isFixed ? 'fixed animate__fadeInDown' : 'relative'">

        <div class="relative bg-black">
            <div class="max-w-screen-2xl mx-auto px-4 sm:px-6">
                <div class="flex justify-between items-center md:justify-start md:space-x-10 text-white py-1 md:py-6">

                    <h1 class="mr-6" :class="isFixed ? 'md:inline-block' : 'md:hidden'"><img src="{{ asset('img/logo-full.svg') }}" alt="Pick Food" class="w-16 h-16"></h1>

                    <div class="" data-aos="fade-in">
                        
                        <a href="https://wa.link/desj4k" target="_blank" title="WhatsApp" class="flex">
                            <svg class="w-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><g data-name="wp-group"><path d="M13.6 2.32A7.85 7.85 0 008 0a7.94 7.94 0 00-6.88 11.89L0 16l4.2-1.1a7.93 7.93 0 003.79 1H8a8 8 0 008-7.93 8 8 0 00-2.4-5.65zM8 14.53a6.58 6.58 0 01-3.36-.92l-.24-.15-2.49.66.66-2.43-.16-.25a6.6 6.6 0 0110.25-8.17 6.65 6.65 0 012 4.66A6.66 6.66 0 018 14.53zm3.61-4.94c-.2-.1-1.17-.58-1.35-.64s-.32-.1-.45.1a9 9 0 01-.63.77c-.11.14-.23.15-.43 0a5.33 5.33 0 01-2.69-2.3c-.21-.35.2-.33.58-1.08a.38.38 0 000-.35C6.57 6 6.17 5 6 4.62s-.32-.33-.45-.34h-.36a.71.71 0 00-.53.25A2.19 2.19 0 004 6.17a3.82 3.82 0 00.81 2.05 8.89 8.89 0 003.39 3 3.85 3.85 0 002.38.5 2 2 0 001.33-.94 1.62 1.62 0 00.09-.95c-.06-.09-.19-.14-.39-.24z" fill="currentColor" data-name="wp"/></g></svg>
                            11 7362 8941</a>
                        {{-- <a href="/iniciar-sesion" class="flex p-4">
                            <svg class="w-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 12.25"><g data-name="Capa 2"><path d="M13 12h-2.62a.38.38 0 01-.38-.37v-1.25a.38.38 0 01.38-.38H13a1 1 0 001-1V3a1 1 0 00-1-1h-2.62a.38.38 0 01-.38-.37V.38a.38.38 0 01.38-.38H13a3 3 0 013 3v6a3 3 0 01-3 3zm-1.47-6.28L6.28.47A.75.75 0 005 1v3H.75a.74.74 0 00-.75.75v3a.74.74 0 00.75.75H5v3a.75.75 0 001.28.5l5.25-5.25a.75.75 0 000-1.03z" fill="#fff" data-name="Capa 1"/></g></svg>
                            Iniciar sesión
                        </a>

                        <a href="/registro" class="p-4">Registrarse</a> --}}
                    </div>

                    <!--Toggle button (hidden on large screens)-->
                    <button
                    @click="isOpen = !isOpen"
                    type="button"
                    aria-controls="main-menu"
                    class="block md:hidden px-2 text-white hover:text-gray-200 focus:outline-none focus:text-gray-200"
                    :class="{ 'transition transform-180': isOpen }"
                    >
                    <svg
                        class="h-6 w-6 fill-current"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <path
                        x-show="isOpen"
                        fill-rule="evenodd"
                        clip-rule="evenodd"
                        d="M18.278 16.864a1 1 0 0 1-1.414 1.414l-4.829-4.828-4.828 4.828a1 1 0 0 1-1.414-1.414l4.828-4.829-4.828-4.828a1 1 0 0 1 1.414-1.414l4.829 4.828 4.828-4.828a1 1 0 1 1 1.414 1.414l-4.828 4.829 4.828 4.828z"
                        />
                        <path
                        x-show="!isOpen"
                        fill-rule="evenodd"
                        d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
                        />
                    </svg>
                    </button>

                    <div class="hidden md:flex items-center justify-end space-x-8 md:flex-1 lg:w-0">
                        <p>Envíos a <strong>CABA, Zona Norte y Sur</strong></p>
                    </div>

                </div>
            </div>

        </div>

        <div class="relative bg-black md:bg-transparent md:block transition-all"
                x-show.transition="true"
                :class="{
                    'shadow-lg show' : isOpen,
                    'hidden' : !isOpen,
                    'md:bg-fucsia' : isFixed }"
        >
            <div class="max-w-7xl mx-auto px-4 sm:px-6 transition-all duration-500 ease-in-out md:opacity-100 md:translate-y-0"
                        :class="isOpen ? 'opacity-100 translate-y-6': 'opacity-0 translate-y-0' "
            >

                <nav id="main-menu">

                    <ul class="flex flex-col md:flex-row justify-between items-center py-6 md:py-4 text-white">

                        <li><a href="#como-funciona" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                            >Cómo funciona</a></li>

                        <li><a href="#menu" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                            >Platos</a></li>

                        {{-- <li><a href="{{ route('nosotros') }}" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150 {{ $section == "nosotros" ? 'text-secondary underline' : '' }}" data-aos="fade-in"
                        >Nosotros</a></li>

                        <li><a href="{{ route('dudas') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150 {{ $section == "dudas" ? 'text-secondary underline' : '' }}" data-aos="fade-in"
                        >Dudas</a></li>

                        <li><a href="{{ route('contacto') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150 {{ $section == "contacto" ? 'text-secondary underline' : '' }}" data-aos="fade-in"
                        >Contacto</a></li> --}}
                        
                        <li class="hidden md:inline-block"
                            :class="{ 'md:hidden' : isFixed }">
                            <h1><img src="{{ asset('img/logo-full.svg') }}" alt="Pick Food"
                                :class="isFixed ? 'hidden' : 'w-32 h-32'"></h1>
                        </li>


                        {{-- <li><a href="{{ route('platos') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150 {{ $section == "platos" ? 'text-secondary underline' : '' }}" data-aos="fade-in"
                        >Platos</a></li>

                        <li><a href="{{ route('carrito') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150 {{ $section == "carrito" ? 'text-secondary underline' : '' }}" data-aos="fade-in"
                        >Carrito</a></li> --}}

                        <li><a href="#envios" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                            >Envíos</a></li>

                        <li>
                            <a class="navbar-item md:py-0  flex items-center" href="https://www.facebook.com/elegipickfood" target="_blank">
                                <svg class="w-4 h-4 md:mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16.1"><defs><clipPath id="a"><path fill="none" d="M0 0h16v16.1H0z"/></clipPath></defs><g data-name="Capa 2"><g clip-path="url(#a)" data-name="Capa 1"><path d="M14.82 0H1.18A1.17 1.17 0 000 1.15V15a1.17 1.17 0 001.18 1.1h3.56v-5.72H3.19V7.62h1.55V5.83A3 3 0 018 2.55h2.4v2.69H8.67A.66.66 0 008 6v1.62h2.47l-.28 2.76H8v5.72h6.83A1.17 1.17 0 0016 15V1.15A1.17 1.17 0 0014.82 0" fill="#fff"/></g></g></svg>
                            </a>
                        </li>
                        <li>
                            <a class="navbar-item md:py-0  flex items-center" href="https://www.instagram.com/elegipickfood/" target="_blank">
                                <svg class="w-4 h-4 md:mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><defs><clipPath id="clip-path"><path fill="none" d="M0 0h16v16H0z"/></clipPath><style>.cls-3{fill:#fff}</style></defs><g id="Capa_2" data-name="Capa 2"><g clip-path="url(#clip-path)" id="Capa_1-2" data-name="Capa 1"><path class="cls-3" d="M11.77 16h-7.5L4 15.94a4.68 4.68 0 01-2.43-1A4.56 4.56 0 010 11.37v-6.7a4.57 4.57 0 011.6-3.58A4.74 4.74 0 014.67 0h6.7a4.55 4.55 0 013.68 1.69A5.32 5.32 0 0116 4.27v7.5l-.06.35a4.22 4.22 0 01-3 3.63 10.84 10.84 0 01-1.13.25M8.06 1.58H4.74a2.94 2.94 0 00-3.15 3.15c-.05 2.19-.05 4.39 0 6.58a2.92 2.92 0 003.08 3.13q3.36.09 6.7 0a2.9 2.9 0 003.08-3.06q.09-3.36 0-6.74a2.91 2.91 0 00-3.12-3H8.06"/><path class="cls-3" d="M3.85 8a4.17 4.17 0 018.34 0 4.17 4.17 0 01-8.34 0m1.57 0A2.6 2.6 0 108 5.42 2.62 2.62 0 005.42 8M12.28 4.77a1 1 0 01-1-1 1 1 0 011-1 1.06 1.06 0 011 1 1 1 0 01-1 1"/></g></g></svg>
                            </a>
                        </li>
                    </ul>

                </nav>

            </div>

        </div>

    </header>
</div>