<button {{ $attributes->merge(['type' => '', 'class' => 'btn btn-primary block']) }}
    ><span class="btn-text">{{ $slot }}</span></button>