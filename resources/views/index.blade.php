<x-front-layout>

    <x-slot name="headtags">
        <title>Pick Food | Comida envasada al vacío. Platos ricos en 15 minutos!</title>
        <meta name="description" content="Tené siempre comidas ricas en el freezer. No tenés tiempo para cocinar? Cansado del delivery? Pick Food te salva el 100% de las veces. Hacé tu pedido semanal.">
    </x-slot>


    <div class="overflow-hidden bg-fucsia">

        <x-navbar section="{{ url()->current() }}" />

        <main class="relative overflow-hidden">

            <img class="transform top-0 -translate-y-2/4 md:left-0 md:translate-y-0 md:-translate-x-2/3 lg:-translate-x-1/2 absolute" src="{{ asset('img/ribs-con-papas.png') }}" alt="Costillas de cerdo con papas">
            <p class="text-black font-script hidden md:block caption-plato-left">Ribs de cerdo con papas bravas</p>


            <img class="transform bottom-0 translate-y-2/4 md:translate-y-0 md:right-0 md:translate-x-2/3 lg:translate-x-2/4 absolute" src="{{ asset('img/wok-soja-veggie.png') }}" alt="Wok de soja con verdura">
            <p class="text-black font-script hidden md:block caption-plato-right">Wok de soja</p>

            <div class="text-center text-white uppercase transition-all p-6 md:px-0 flex flex-col justify-center items-center banner-strip-end">

                <h2 class="font-barlow font-bold text-3xl md:text-4xl lg:text-6xl md:max-w-md lg:max-w-3xl mb-8" data-aos="fade-up">Tené siempre comidas reales en minutos en tu mesa. <span class="block text-sm md:text-4xl lg:text-6xl">La solución para tu almuerzo o cena</span></h2>

                <a href="#menu" id="cta-vermenu" class="btn btn-primary" data-aos="fade-up" data-aos-delay="300" data-aos-anchor=".banner-strip-end"><span class="btn-text">Mirá el menú</span></a>
            </div>
            
        </main>


    </div>


    <section id="como-funciona" class="container m-auto -mt-12 mb-24">
        
        <div class="text-center max-w-xl m-auto py-12 px-6 md:px-0">
            <h3 class="font-barlow font-bold text-4xl md:text-6xl mb-8 text-black uppercase pt-12 md:pt-0">¿Cómo funciona?</h3>
            <p class="font-bold md:text-xl">¿Sin tiempo de cocinar, estás a full con el laburo o la facu? Te proponemos otra forma de comer, olvidate del delivery.</p>
        </div>

        <ol class="md:flex pasos fucsia">

            <li class="md:w-1/3 mb-24">
                <div class="relative z-10">
                    <svg class="w-16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39 52"><g data-name="Capa 2"><path d="M13.78 0a4.58 4.58 0 014.45 4.55v10.14a5 5 0 017.2.64A4.65 4.65 0 0131.76 17c5-.91 7.24 2.23 7.24 7.37V26c0 6.29-3.15 7.81-3.89 12.57a2.43 2.43 0 01-2.41 2H15.26a4.87 4.87 0 01-4.45-2.89C9.49 34.8 5.83 28 3 26.81a4.33 4.33 0 01-3-4.06 4.88 4.88 0 016.8-4.48 13.57 13.57 0 012.53 1.44V4.55A4.65 4.65 0 0113.78 0zm0 42.25h19.5a2.44 2.44 0 012.44 2.44v4.87A2.44 2.44 0 0133.31 52h-19.5a2.43 2.43 0 01-2.43-2.44v-4.87a2.43 2.43 0 012.43-2.44zm17.07 2.84a2 2 0 102 2 2 2 0 00-1.97-2z" data-name="Capa 1"/></g></svg>
                    <p><strong>Abrís el freezer y elegís</strong> cuál de nuestros platos vas a degustar.</p>
                </div>
            </li>

            <li class="md:w-1/3 mb-24">
                <div class="relative z-10">
                    <svg class="w-16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><g data-name="Capa 2"><path d="M42.25 0A9.75 9.75 0 1052 9.75 9.77 9.77 0 0042.25 0zm0 13a3.25 3.25 0 113.25-3.25A3.24 3.24 0 0142.25 13zM26 11.38a11.38 11.38 0 10-22.75 0v16.91a14.6 14.6 0 1026 9.09A14.47 14.47 0 0026 28.29zM14.63 45.5a8.11 8.11 0 01-4.88-14.6V11.38a4.88 4.88 0 119.75 0V30.9a8.11 8.11 0 01-4.87 14.6zm1.62-12.71V11.38a1.63 1.63 0 10-3.25 0v21.41a4.88 4.88 0 103.25 0z" data-name="Capa 1"/></g></svg>
                    <p><strong>Ponés a hervir agua en una olla grande.</strong> Cuando arrancan las burbujas ponés el timer y metés la bolsa cerrada el tiempo que dice la etiqueta. No hagas trampa ;)</p>
                </div>
            </li>

            <li class="md:w-1/3 mb-24">
                <div class="relative z-10">
                    <svg class="w-16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.25 52"><g data-name="Capa 2"><path d="M21.11 1.54c.09.48 1.64 9.6 1.64 13.09 0 5.31-2.82 9.1-7 10.62l1.31 24.18A2.44 2.44 0 0114.62 52h-6.5a2.44 2.44 0 01-2.43-2.57L7 25.25c-4.19-1.52-7-5.32-7-10.62C0 11.13 1.55 2 1.64 1.54 2-.52 6.24-.55 6.5 1.66V16c.13.34 1.53.32 1.62 0 .15-2.57.81-14.14.82-14.41.33-2.11 4.54-2.11 4.86 0 0 .28.67 11.84.81 14.41.1.32 1.51.34 1.63 0V1.66c.26-2.2 4.55-2.18 4.87-.12zm12.11 29L31.7 49.36A2.43 2.43 0 0034.12 52h5.69a2.44 2.44 0 002.44-2.44V2.44A2.44 2.44 0 0039.81 0c-8.38 0-22.48 18.13-6.59 30.56z" data-name="Capa 1"/></g></svg>
                    <p><strong>¡Ahora viene lo mejor!</strong> Sacá la bolsa con cuidado, abrila y serví en tu plato.</p>
                </div>
            </li>

        </ol>

    </section>


    <section id="promo-envio" class="bg-verde bg-no-repeat bg-bottom bg-contain lg:bg-cover bg-envios-sm md:bg-envios-md lg:bg-envios-lg h-100">

        <div class="container mx-auto md:flex md:justify-end pt-12 px-6 md:p-0 md:items-center h-full">
            <div class="md:w-2/5 text-white">
                <p class="font-bold flex text-xl mb-2">
                    <svg class="w-6 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Capa 2"><path d="M24 12A12 12 0 1112 0a12 12 0 0112 12zm-12 2.42a2.23 2.23 0 102.23 2.23A2.23 2.23 0 0012 14.42zm-2.11-8l.36 6.58a.57.57 0 00.58.55h2.34a.57.57 0 00.58-.55l.36-6.58a.58.58 0 00-.58-.61h-3.06a.58.58 0 00-.58.61z" fill="#fff" data-name="importante"/></g></svg>
                    Promo lanzamiento
                </p>
                <h3 class="font-barlow font-bold text-3xl md:text-5xl mb-4 md:mb-8 uppercase">¡Si pedís 5 platos o más tenés envío gratis!</h3>
                <img src="{{ asset('img/underline.svg') }}" alt="subrayado" class="w-64">
            </div>
        </div>


    </section>


    {{-- menu platos --}}
    <livewire:platos-home />


    <section class="bg-amarillo">

        <div class="text-center max-w-xl m-auto pt-12 md:py-12 text-white">
            <h3 class="font-barlow font-bold text-4xl md:text-6xl mb-2 md:mb-8 uppercase">¿Cómo pedir?</h3>
            <p class="p-6 max-w-sm md:max-w-xl">Hacé tu pedido por WhatsApp al <strong>11 7362 8941</strong></p>
        </div>

        <div class="container mx-auto md:flex mb-6">
            <ol class="md:flex pasos blanco">

                <li class="mb-12 md:w-1/3">
                    <div class="relative z-10">
                        <svg class="w-12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><g data-name="wp"><path d="M25 10.13a14.72 14.72 0 00-12.47 22.54l.35.56-1.48 5.42 5.6-1.46.54.32A14.64 14.64 0 0025 39.56a14.87 14.87 0 0014.88-14.71 14.79 14.79 0 00-4.48-10.4A14.59 14.59 0 0025 10.13zm8.65 21a4.5 4.5 0 01-3 2.1c-1.41.21-2.5.1-5.3-1.11-4.43-1.92-7.33-6.38-7.56-6.67A8.6 8.6 0 0116 20.9a4.93 4.93 0 011.56-3.69 1.59 1.59 0 011.18-.56h.85c.26 0 .63-.1 1 .76s1.25 3.06 1.36 3.28a.8.8 0 010 .77c-.85 1.7-1.76 1.63-1.3 2.41 1.71 2.94 3.42 3.95 6 5.26.44.22.7.19 1-.11s1.1-1.3 1.39-1.73.59-.37 1-.22 2.58 1.21 3 1.44.74.33.85.51a3.92 3.92 0 01-.24 2.14zM44.64 0H5.36A5.36 5.36 0 000 5.36v39.28A5.36 5.36 0 005.36 50h39.28A5.36 5.36 0 0050 44.64V5.36A5.36 5.36 0 0044.64 0zM25 42.54a17.64 17.64 0 01-8.46-2.15l-9.4 2.47 2.51-9.18a17.73 17.73 0 1133.21-8.83A17.86 17.86 0 0125 42.54z" data-name="pedir por whatsapp"/></g></svg>
                        <p>Elegí todos los platos que quieras de nuestro menú y mandarlo por <strong>WhatsApp o nuestras redes</strong>.</p>
                    </div>
                </li>
    
                <li class="mb-12 md:w-1/3">
                    <div class="relative z-10">
                        <svg class="w-12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 46.67"><g data-name="credito"><path d="M0 41.67a5 5 0 005 5h50a5 5 0 005-5V23.33H0zm20-7.09a1.26 1.26 0 011.25-1.25h14.17a1.26 1.26 0 011.25 1.25v4.17A1.26 1.26 0 0135.42 40H21.25A1.25 1.25 0 0120 38.75zm-13.33 0a1.26 1.26 0 011.25-1.25h7.5a1.26 1.26 0 011.25 1.25v4.17A1.26 1.26 0 0115.42 40h-7.5a1.25 1.25 0 01-1.25-1.25zM60 5v5H0V5a5 5 0 015-5h50a5 5 0 015 5z" data-name="tarjetas"/></g></svg>
                        <p><strong>Podés pagar con tarjeta de crédito o débito</strong> a través de MercadoPago o por transferencia bancaria.</p>
                    </div>
                </li>
    
                <li class="mb-12 md:w-1/3">
                    <div class="relative z-10">
                        <svg class="w-12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47 53.71"><g data-name="dias"><path d="M0 48.68a5 5 0 005 5h37a5 5 0 005-5V20.14H0zm6.71-20.14a1.68 1.68 0 011.68-1.68h10.07a1.68 1.68 0 011.68 1.68v10.07a1.68 1.68 0 01-1.68 1.68H8.39a1.69 1.69 0 01-1.68-1.68zM42 6.71h-5v-5A1.69 1.69 0 0035.25 0h-3.36a1.69 1.69 0 00-1.68 1.68v5H16.79v-5A1.69 1.69 0 0015.11 0h-3.36a1.69 1.69 0 00-1.68 1.68v5H5a5 5 0 00-5 5v5h47v-5a5 5 0 00-5-4.97z" data-name="calendario"/></g></svg>
                        <p>Enviamos todos los <strong>jueves (zona norte) y viernes (CABA y zona sur)</strong> de 16 a 20.30 hs.</p>
                    </div>
                </li>
    
            </ol>
        </div>

        <div class="container mx-auto md:flex">
            {{-- lock --}}
            <div class="md:w-1/3 md:pr-6">
                <div class="rounded-t-3xl bg-black text-white relative pt-6 pb-12 px-12 h-full md:py-6">
                    <div class="absolute -top-6 right-5 rounded-full bg-white">
                        <svg class="w-16 p-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35 40"><g data-name="Capa 2"><path d="M31.25 17.5h-1.87v-5.62a11.88 11.88 0 10-23.75 0v5.62H3.75A3.75 3.75 0 000 21.25v15A3.75 3.75 0 003.75 40h27.5A3.75 3.75 0 0035 36.25v-15a3.75 3.75 0 00-3.75-3.75zm-8.12 0H11.88v-5.62a5.63 5.63 0 1111.25 0z" fill="#00d39c" data-name="Capa 1"/></g></svg>
                    </div>
                    <p>Compra online protegida.</p>
                </div>
            </div>
            
            {{-- mercado pago --}}
            <div class="md:w-2/3">
                <div class="md:rounded-t-3xl bg-black text-white relative pt-12 pb-6 md:py-6 px-12 border-amarillo border-t-2 md:border-none">
                    <div class="absolute -top-6 right-5 rounded-full bg-white">
                        <img class="w-16 p-2" src="{{ asset('img/mp.png') }}" alt="Mercado Pago">
                    </div>
                    <p class="w-11/12">Comprá fácil con tarjeta de crédito o débito, en cuotas o en efectivo. Aceptamos muchos medios de pago a través de Mercado Pago.</p>
                </div>
            </div>

        </div>
    </section>


    <section id="envios" class="md:flex h-1/2 relative overflow-hidden">

        <div class="md:w-1/2 -mt-12">
            <iframe class="w-full h-96 md:h-full" src="https://www.google.com/maps/d/embed?mid=1QJBufmgH79Gn8apATaaTfglRPyDeyH7o&z=10" width="640" height="480"></iframe>
        </div>

        <div class="flex flex-col md:w-1/2 bg-white text-black">

            <div class="p-12">
                <h3 class="font-barlow font-bold text-6xl mb-8 text-amarillo uppercase">Envíos</h3>
                <p class="text-lg font-bold uppercase">Jueves</p>
                <h4 class="mb-4"><strong class="text-xl">GBA Zona norte</strong><br>$305</h4>
    
                <p class="text-lg font-bold uppercase">Viernes</p>
                <h4 class="mb-4"><strong class="text-xl">CABA</strong><br>$125</h4>
                <h4 class="mb-4"><strong class="text-xl">GBA Zona sur</strong><br>$305</h4>
            </div>

            <img class="self-end w-max-xs md:w-2/3 lg:w-1/4 lg:absolute lg:bottom-0 right-0" src="{{ asset('img/recibi-pack-pedido.png') }}" alt="pack comidas semana entrega">
        </div>
        

    </section>


    <footer>
        <div class="bg-fucsia text-white p-12">
            <div class="container mx-auto md:flex">
                <div class="mb-6 md:w-3/5 md:mb-0">
                    <div class="md:flex items-end">
                        <img src="{{ asset('img/logo-full.svg') }}" alt="Pick Food" class="w-24 h-24 mb-4 md:mb-0 md:mr-6">

                        <div>
                            <p class="flex">
                                <a class="flex items-center" href="https://www.facebook.com/elegipickfood" target="_blank">
                                    <svg class="w-4 h-4 mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16.1"><defs><clipPath id="a"><path fill="none" d="M0 0h16v16.1H0z"/></clipPath></defs><g data-name="Capa 2"><g clip-path="url(#a)" data-name="Capa 1"><path d="M14.82 0H1.18A1.17 1.17 0 000 1.15V15a1.17 1.17 0 001.18 1.1h3.56v-5.72H3.19V7.62h1.55V5.83A3 3 0 018 2.55h2.4v2.69H8.67A.66.66 0 008 6v1.62h2.47l-.28 2.76H8v5.72h6.83A1.17 1.17 0 0016 15V1.15A1.17 1.17 0 0014.82 0" fill="#fff"/></g></g></svg>
                                </a>
    
                                <a class="flex items-center" href="https://www.instagram.com/elegipickfood/" target="_blank">
                                    <svg class="w-4 h-4 mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><defs><clipPath id="clip-path"><path fill="none" d="M0 0h16v16H0z"/></clipPath><style>.cls-3{fill:#fff}</style></defs><g id="Capa_2" data-name="Capa 2"><g clip-path="url(#clip-path)" id="Capa_1-2" data-name="Capa 1"><path class="cls-3" d="M11.77 16h-7.5L4 15.94a4.68 4.68 0 01-2.43-1A4.56 4.56 0 010 11.37v-6.7a4.57 4.57 0 011.6-3.58A4.74 4.74 0 014.67 0h6.7a4.55 4.55 0 013.68 1.69A5.32 5.32 0 0116 4.27v7.5l-.06.35a4.22 4.22 0 01-3 3.63 10.84 10.84 0 01-1.13.25M8.06 1.58H4.74a2.94 2.94 0 00-3.15 3.15c-.05 2.19-.05 4.39 0 6.58a2.92 2.92 0 003.08 3.13q3.36.09 6.7 0a2.9 2.9 0 003.08-3.06q.09-3.36 0-6.74a2.91 2.91 0 00-3.12-3H8.06"/><path class="cls-3" d="M3.85 8a4.17 4.17 0 018.34 0 4.17 4.17 0 01-8.34 0m1.57 0A2.6 2.6 0 108 5.42 2.62 2.62 0 005.42 8M12.28 4.77a1 1 0 01-1-1 1 1 0 011-1 1.06 1.06 0 011 1 1 1 0 01-1 1"/></g></g></svg>
                                </a>
    
                                @elegipickfood</p>

                                <a href="mailto:hola@pickfood.com.ar">hola@pickfood.com.ar</a>
                        </div>

                    </div>

                </div>

                <ul class="w-1/5">

                    <li><a href="#como-funciona" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                        >Cómo funciona</a></li>

                    <li><a href="#menu" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                        >Platos</a></li>

                    {{-- <li><a href="{{ route('nosotros') }}" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                        >Nosotros</a></li>

                    <li><a href="{{ route('dudas') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150" data-aos="fade-in"
                    >Dudas</a></li>

                    <li><a href="{{ route('contacto') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150" data-aos="fade-in"
                        >Contacto</a></li> --}}
                </ul>

                <ul class="w-1/5">
                    <li><a href="#envios" class="navbar-item md:py-0 focus:outline-none ease-in-out duration-150" data-aos="fade-in"
                        >Envíos</a></li>

                    {{-- <li><a href="{{ route('carrito') }}" class="navbar-item md:py-0 focus:outline-none transition ease-in-out duration-150" data-aos="fade-in"
                        >Carrito</a></li> --}}
                </ul>
            </div>
        </div>

        <div class="bg-black text-center text-white p-6">
            <p>Cocinamos por vos con ♥</p>
            <p class="text-gray-400 text-sm">Desarrollado por <a href="https://prismadesign.com.ar" target="_blank">Prisma Design</a></p>
        </div>
</footer>


<x-slot name="scripts">
    <script>
        (function () {
            document.getElementById('cta-vermenu').addEventListener('click', function(){
                fbq('trackCustom', 'vermenu');
            });
            document.getElementById('cta-masplatos').addEventListener('click', function(){
                fbq('trackCustom', 'vermenu');
            });
            document.getElementById('cta-hacerpedido').addEventListener('click', function(){
                fbq('trackCustom', 'hacerpedido');
            });
        })();
    </script>
</x-slot>
    

</x-front-layout>