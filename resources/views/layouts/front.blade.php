<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{ $headtags }}

        <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

        <x-fbpixel />
        <x-gatag />

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@800&family=Inter:wght@400;700;800&family=Kalam&display=swap" rel="stylesheet">
        
        @livewireStyles

        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.0/dist/alpine.min.js" defer></script>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    </head>
    <body class="antialiased">


        {{ $slot }}


        <x-social-bar />


        {{-- <livewire:policies-accept /> --}}

        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
            AOS.init({
                duration: 600,
                easing: 'ease-in-out',
            });
          </script>

          @if ($scripts) {{ $scripts }} @endif

        @livewireScripts

    </body>
</html>
