<section id="menu" class="py-24">

    <span class="hidden bg-violeta"></span>

    <div class="container mx-auto">

        <div class="text-center">
            <h3 class="font-barlow font-bold text-4xl md:text-5xl uppercase mb-4">elegí los platos que más te gusten</h3>

            <p class="font-script text-lg m-auto px-6 max-w-md mb-6">Proximamente vas a poder comprar desde nuestra web! Por ahora escribinos por WhatsApp</p>

            <a href="https://wa.link/desj4k" target="_blank" class="btn btn-primary btn-shadow-amarillo block" data-aos="fade-up" data-aos-delay="300" data-aos-anchor="#menu" id="cta-hacerpedido"><span class="btn-text">Hacé tu pedido</span></a>
        </div>

        <div class="px-6 md:px-0 md:grid md:grid-cols-2 xl:grid-cols-4 md:gap-6 mt-24">


            @foreach ($platos as $plato)
                
                <article class="bg-{{ $plato->categoria->color }} card-plato">

                    <div class="plato-cont">
                        <img class="plate absolute z-1" src="{{ asset('img/plato-logo.svg') }}" alt="">
                        <img class="hero-plato" src="{{ asset('storage/platos/'.$plato->photo_path) }}" alt="{{ $plato->alt }}">
                    </div>

                    <p class="font-script text-lg border-b border-white mb-2">
                        {{ $plato->categoria->nombre }}
                    </p>
                    <h4 class="font-bold text-2xl">{{ $plato->nombre }}</h4>
                    <h5 class="text-xl">${{ $plato->precio }}</h5>
                </article>

            @endforeach

           
        </div>
    </div>

    @if ($limit)
        <div class="text-center py-12 btn-platos-cont">
            <button wire:click="more" id="cta-masplatos" class="btn btn-primary btn-shadow-fucsia aos-init aos-animate"
                data-aos="fade-in" data-aos-anchor=".btn-platos-cont">
                <span class="btn-text">Conocé todos nuestros platos</span>
            </button>
        </div>
    @endif

</section>
