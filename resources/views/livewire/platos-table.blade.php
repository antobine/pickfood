<div>

	@php
		$routeName = 'platos';
		$mName = 'plato';
	@endphp

	<x-slot name="headtags">
		<title>{{ Str::ucfirst($routeName) }} - {{ config('app.name', 'Pick Food') }}</title>
	</x-slot>

    <x-slot name="header">
        <h1>{{ Str::ucfirst($routeName) }}</h1>
    </x-slot>

    <!-- component -->
<div class="antialiased sans-serif bg-gray-200 h-screen">
	

	<div class="container mx-auto py-6 px-4" x-data="datatables()" x-cloak>

        <div class="mb-4 flex justify-between items-center">
            <x-button href="{{ route( $routeName .'.create') }}" class="btn-shadow-amarillo">Nuevo {{ $mName }}</x-button>
		</div>
		
		@if ( isset($message))
			{{ $message }}
		@endif


		<div x-show="selectedRows.length" class="bg-teal-200 fixed top-0 left-0 right-0 z-40 w-full shadow">
			<div class="container mx-auto px-4 py-4">
				<div class="flex md:items-center">
					<div class="mr-4 flex-shrink-0">
						<svg class="h-8 w-8 text-teal-600"  viewBox="0 0 20 20" fill="currentColor">  <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"/></svg>
					</div>
					<div x-html="selectedRows.length + ' rows are selected'" class="text-teal-800 text-lg"></div>
				</div>
			</div>
		</div>

		<div class="mb-4 flex justify-between items-center">
			<div class="flex-1 pr-4">
				<div class="relative md:w-1/3">
					<input type="search"
						class="w-full pl-10 pr-4 py-2 rounded-lg shadow focus:outline-none focus:shadow-outline text-gray-600 font-medium"
						placeholder="Search...">
					<div class="absolute top-0 left-0 inline-flex items-center p-2">
						<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-400" viewBox="0 0 24 24"
							stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
							stroke-linejoin="round">
							<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
							<circle cx="10" cy="10" r="7" />
							<line x1="21" y1="21" x2="15" y2="15" />
						</svg>
					</div>
				</div>
			</div>
			<div>
				<div class="shadow rounded-lg flex">
					<div class="relative">
						<button @click.prevent="open = !open"
							class="rounded-lg inline-flex items-center bg-white hover:text-blue-500 focus:outline-none focus:shadow-outline text-gray-500 font-semibold py-2 px-2 md:px-4">
							<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 md:hidden" viewBox="0 0 24 24"
								stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
								stroke-linejoin="round">
								<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
								<path
									d="M5.5 5h13a1 1 0 0 1 0.5 1.5L14 12L14 19L10 16L10 12L5 6.5a1 1 0 0 1 0.5 -1.5" />
							</svg>
							<span class="hidden md:block">Display</span>
							<svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 ml-1" width="24" height="24"
								viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
								stroke-linecap="round" stroke-linejoin="round">
								<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
								<polyline points="6 9 12 15 18 9" />
							</svg>
						</button>

						<div x-show="open" @click.away="open = false"
							class="z-40 absolute top-0 right-0 w-40 bg-white rounded-lg shadow-lg mt-12 -mr-1 block py-1 overflow-hidden">
							<template x-for="heading in headings">
								<label
									class="flex justify-start items-center text-truncate hover:bg-gray-100 px-4 py-2">
									<div class="text-teal-600 mr-3">
										<input type="checkbox" class="form-checkbox focus:outline-none focus:shadow-outline" checked @click="toggleColumn(heading.key)">
									</div>
									<div class="select-none text-gray-700" x-text="heading.value"></div>
								</label>
							</template>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="overflow-x-auto bg-white rounded-lg shadow overflow-y-auto relative">
			<table class="border-collapse table-auto w-full whitespace-wrap bg-white table-striped relative">
				<thead>
					<tr class="text-left">
						<th class="py-2 px-3 sticky top-0 border-b border-gray-200 bg-gray-100">
							<label
								class="text-teal-500 inline-flex justify-between items-center hover:bg-gray-200 px-2 py-2 rounded-lg cursor-pointer">
								<input type="checkbox" class="form-checkbox focus:outline-none focus:shadow-outline" @click="selectAllCheckbox($event);">
							</label>
						</th>
						<template x-for="heading in headings">
							<th class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs"
								x-text="heading.value" :x-ref="heading.key" :class="{ [heading.key]: true }"></th>
						</template>
					</tr>
                </thead>
                
				<tbody>

                    @foreach ($platos as $plato)
                        
						<tr>
							<td class="border-dashed border-t border-gray-200 px-3">
								<label
									class="text-teal-500 inline-flex justify-between items-center hover:bg-gray-200 px-2 py-2 rounded-lg cursor-pointer">
									<input type="checkbox" class="form-checkbox rowCheckbox focus:outline-none focus:shadow-outline" :name="{{ $plato->id }}"
											@click="getRowDetail($event, {{ $plato->id }})">
								</label>
							</td>
							<td class="border-dashed border-t border-gray-200 id">
								<span class="text-gray-700 px-6 py-3 flex items-center">{{ $plato->id }}</span>
							</td>
							<td class="border-dashed border-t border-gray-200 photo">
								<img src="{{ asset('storage/'.$routeName.'/'.$plato->photo_path ) }}" alt="" class="max-w-xs">
							</td>
							<td class="border-dashed border-t border-gray-200 nombre">
								<span class="text-gray-700 px-6 py-3 flex items-center font-bold">{{ $plato->nombre }}
								@if ($plato->is_destacado)
								<div class="w-6 h-6">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
										<path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
									</svg>
								</div>
								@endif
								</span>
							</td>
							
                            <td class="border-dashed border-t border-gray-200 category_id">
								<span class="bg-{{ $plato->categoria->color }} text-white text-sm rounded px-4 py-2 inline-block mr-2 mb-2">
									{{  $plato->categoria->nombre }}
								</span>
							</td>
							<td class="border-dashed border-t border-gray-200 precio">
								<span class="text-gray-700 px-6 py-3 flex items-center">{{ $plato->precio }}</span>
							</td>

							<td>
								<div class="flex flex-col h-full">
									<a href="/{{ $routeName }}" target="_blank" class="p-4" title="Ver online">
										<svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
											<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
										</svg>
									</a>
									
									<a href="{{ route($routeName.'.edit', $plato->id) }}" class="p-4" title="Editar">
										<svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
											<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
										</svg>
									</a>
		
									<form action="/{{ $routeName }}/{{ $plato->id }}" method="POST">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button class="p-4">
											<svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
												<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
											</svg>
										</button>
									</form>
								</div>
							</td>
							
						</tr>
                    
                    @endforeach

				</tbody>
			</table>
		</div>
	</div>

	<x-slot name="scripts">
		<script>
			function datatables() {
				return {
					headings: [
						{
							'key': 'id',
							'value': 'ID'
						},
						{
							'key': 'photo',
							'value': 'Foto'
						},
						{
							'key': 'nombre',
							'value': 'nombre'
						},
						{
							'key': 'categoria',
							'value': 'categoria'
						},
						{
							'key': 'precio',
							'value': 'precio'
						},
						{
							'key': 'actions',
							'value': '-'
						}
					],

					selectedRows: [],

					open: false,
					
					toggleColumn(key) {
						// Note: All td must have the same class name as the headings key! 
						let columns = document.querySelectorAll('.' + key);

						if (this.$refs[key].classList.contains('hidden') && this.$refs[key].classList.contains(key)) {
							columns.forEach(column => {
								column.classList.remove('hidden');
							});
						} else {
							columns.forEach(column => {
								column.classList.add('hidden');
							});
						}
					},

					getRowDetail($event, id) {
						let rows = this.selectedRows;

						if (rows.includes(id)) {
							let index = rows.indexOf(id);
							rows.splice(index, 1);
						} else {
							rows.push(id);
						}
					},

					selectAllCheckbox($event) {
						let columns = document.querySelectorAll('.rowCheckbox');

						this.selectedRows = [];

						if ($event.target.checked == true) {
							columns.forEach(column => {
								column.checked = true
								this.selectedRows.push(parseInt(column.name))
							});
						} else {
							columns.forEach(column => {
								column.checked = false
							});
							this.selectedRows = [];
						}
					}
				}
			}
		</script>
	</x-slot>


</div>