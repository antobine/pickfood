<div>

    <div class="flex relative mb-4">
        <label class="file-label">
            <input class="file-input" type="file" wire:model="photo" name="photo">
            <span class="flex items-center justify-center bg-offwhite border border-solid">

                <svg class="w-8 h-8 p-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                </svg>

                <span class="p-2 pr-4">
                    <span class="file-label-text">
                        @if($photo)
                            {{ Str::limit( $photo->temporaryUrl(), 25 ) }}
                        @else
                            {{ $text }}
                        @endif
                    </span>
                </span>
            </span>
        </label>

    </div>

    @if ($photo)

        <small class="is-muted">Vista previa</small><br>
        <figure class="image is-square rounded-corners mb-5">
            <img src="{{ $photo->temporaryUrl() }}" class="object-cover">
        </figure>

    @endif

    <div wire:loading wire:target="photo">Cargando...</div>

    @error('photo') <span class="error">{{ $message }}</span> @enderror

</div>