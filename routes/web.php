<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PlatoController;
use App\Http\Livewire\PlatosTable;
use App\Http\Livewire\CategoriasTable;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/nosotros', [PageController::class, 'nosotros'])->name('nosotros');
Route::get('/dudas', [PageController::class, 'dudas'])->name('dudas');
Route::get('/contacto', [PageController::class, 'contacto'])->name('contacto');
Route::get('/platos', [PageController::class, 'platos'])->name('platos');
Route::get('/carrito', [PageController::class, 'carrito'])->name('carrito');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/admin/platos', PlatosTable::class)->name('admin.platos');
    Route::get('/admin/categorias', CategoriasTable::class)->name('admin.categorias');

    Route::resource('platos', PlatoController::class, ['except' => ['index', 'show']]);
    Route::resource('categorias', CategoriaController::class, ['except' => ['index', 'show']]);

});
