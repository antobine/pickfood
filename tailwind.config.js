const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            colors: {
                fucsia: 'var(--color-fucsia)',
                verde: 'var(--color-verde)',
                verdeosc: 'var(--color-verde-osc)',
                naranja: 'var(--color-naranja)',
                amarillo: 'var(--color-amarillo)',
                violeta: 'var(--color-violeta)',
            },
            fontFamily: {
                sans: ['Inter', ...defaultTheme.fontFamily.sans],
                script: ['Kalam', 'sans-serif'],
                barlow: ['Barlow', 'sans-serif']
            },
            height: {
                100: '30rem',
                192: '48rem'
            },
            boxShadow: {
                smooth: '0 0.1px 2.9px -17px rgba(0, 0, 0, 0.007), 0 0.3px 7.9px -17px rgba(0, 0, 0, 0.01), 0 0.6px 19px -17px rgba(0, 0, 0, 0.013), 0 2px 63px -17px rgba(0, 0, 0, 0.02)'
            },
            backgroundImage: theme => ({
                'envios-sm': "url('/img/promo-envios-platos-sm.jpg')",
                'envios-md': "url('/img/promo-envios-platos-md.jpg')",
                'envios-lg': "url('/img/promo-envios-platos-lg.jpg')",
            })
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
